section .text

global kernel

disable_cursor:
	mov	dx, 0x3d4
	mov	al, 0x0a
	out	dx, al

	inc	dx
	mov	al, 0x20
	out	dx, al
	ret

; draw status line
statusline:
	mov	edi, 0xb8000 + 80*24*2

	mov	ecx, 80
	mov	ah, 0x70
next_sym:
	lodsb
	or	al, al
	je	spaces
	stosw
	dec	ecx
	jmp	next_sym

spaces:
	mov	ax, 0x7020
	rep stosw
	ret

; kernel entry point
kernel:
	call disable_cursor

	mov	edi, 0xb8000

	cld
	; header
	mov	ax, 0x7020
	mov	ecx, 80
	rep stosw

	; body
	mov	ax, 0x71b0
	mov	ecx, 80*23
	rep stosw

	; footer
	mov	ax, 0x7020
	mov	ecx, 80
	rep stosw

	; status line
	mov	esi, loading
	call statusline

;	mov	esi, init_smth
;	call statusline

	ret

; data
section .data

struc GDT_ENTRY
	gdt_limit_low:   resw 1
	gdt_base_low:    resw 1
	gdt_base_middle: resb 1
	gdt_access:      resb 1
	gdt_granularity: resb 1
	gdt_base_high:   resb 1
endstruc

GDT:
; 3 GDT entries
; NULL descriptor
istruc GDT_ENTRY
	at gdt_limit_low,   dw 0
	at gdt_base_low,    dw 0
	at gdt_base_middle, db 0
	at gdt_access,      db 0
	at gdt_granularity, db 0
	at gdt_base_high,   db 0
iend
istruc GDT_ENTRY
	at gdt_limit_low,   dw 1
	at gdt_base_low,    dw 1
	at gdt_base_middle, db 1
	at gdt_access,      db 1
	at gdt_granularity, db 1
	at gdt_base_high,   db 1
iend
istruc GDT_ENTRY
	at gdt_limit_low,   dw 1
	at gdt_base_low,    dw 1
	at gdt_base_middle, db 1
	at gdt_access,      db 1
	at gdt_granularity, db 1
	at gdt_base_high,   db 1
iend

loading:   db "Initializing memory", 0
init_smth: db "Init aaa!", 0
