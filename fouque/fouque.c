#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#define WORD_MAX_SIZE 128
#define STACK_SIZE (1024*1024)
#define STACK_MAX_ITEMS 1024

struct stack_entry_meta
{
	uint32_t type;
	uint32_t len;

	uint8_t *data; /* pointer to data */
};

/* we're using 2 stacks - for data and metadata */
struct stack
{
	uint8_t *start;  /* data stack start */
	uint8_t *data;   /* pointer to free space */

	size_t nent;     /* number of entries in metadata stack */
	struct stack_entry_meta meta[STACK_MAX_ITEMS];
};

/* vocabulary entry, word with code */
struct voc_entry
{
	char word[WORD_MAX_SIZE];
	void (**code)(struct stack *);
	size_t codesize;
};

struct vocabulary
{
	struct voc_entry *words;
	size_t nwords;
};

static void
plus(struct stack *st)
{
	int64_t *iptr_a, *iptr_b;

	if (st->nent < 2) {
		printf("stack underflow!\n");
		return;
	}

	iptr_a = (int64_t *)st->meta[st->nent - 1].data;
	iptr_b = (int64_t *)st->meta[st->nent - 2].data;

	*iptr_b += *iptr_a;
	st->data -= st->meta[st->nent - 1].len;
	st->nent -= 1;

	printf("plus!\n");
}

static void
voc_init(struct vocabulary *v)
{
	struct voc_entry entry;

	v->words = NULL;
	v->nwords = 0;

	strcpy(entry.word, "+");
	entry.codesize = 1;
	entry.code = malloc(entry.codesize * sizeof(void *));
	entry.code[0] = &plus;

	v->words = realloc(v->words,
		(v->nwords + 1) * sizeof(struct voc_entry));
	v->words[v->nwords] = entry;
	v->nwords++;
}

static void
stack_init(struct stack *st)
{
	st->start = malloc(STACK_SIZE);
	st->data = st->start;

	st->nent = 0;
}

static void
stack_dump(struct stack *st)
{
	int64_t *iptr;
	size_t i;

	printf("> ");
	for (i=0; i<st->nent; i++) {
		switch (st->meta[i].type) {
			case 0:
				iptr = (int64_t *)st->meta[i].data;
				printf("%lu  ", *iptr);
				break;
			default:
				printf("unknown type %u", st->meta[i].type);
				break;
		}
	}
	printf("\n");
}

/* push number to stack */
static void
stack_push_number(struct stack *st, long int n)
{
	int64_t *iptr;

	printf("pushing to stack %ld\n", n);

	st->meta[st->nent].type = 0;
	st->meta[st->nent].len = sizeof(int64_t);
	st->meta[st->nent].data = st->data;

	iptr = (int64_t *)st->meta[st->nent].data;
	*iptr = n;

	st->data += st->meta[st->nent].len;
	st->nent++;
}

int
main()
{
	struct vocabulary voc;
	struct stack st;

	voc_init(&voc);
	stack_init(&st);

	while (!feof(stdin)) {
		char word[WORD_MAX_SIZE];
		int rc;
		size_t i;
		long int num;
		char *endptr;

		rc = scanf("%s", word);
		if (rc == EOF) {
			printf("EOF\n");
			break;
		} else if (rc != 1) {
			printf("?\n");
			break;
		}

		/* if word is number */
		errno = 0;
		num = strtol(word, &endptr, 10);
		if ((word != endptr) && !errno) {
			stack_push_number(&st, num);
		} else {
			int found = 0;

			for (i=0; i<voc.nwords; i++) {
				if (strcmp(word, voc.words[i].word) == 0) {
					size_t j;
					struct voc_entry *word = &voc.words[i];

					found = 1;
					/* execute code */
					for (j=0; j<word->codesize; j++) {
						(*(word->code[j]))(&st);
					}
					break;
				}
			}
			if (!found) {
				printf("unknown word '%s'\n", word);
			}
		}
		stack_dump(&st);
	}
	free(st.start);
	return 0;
}

